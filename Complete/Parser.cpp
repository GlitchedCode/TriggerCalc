#include "Parser.h"

#include <iostream>

Parser::Parser()
{
	// Vuoto.
}


Parser::~Parser()
{
	// Svuota la mappa prima di uscire di scope.
	for (ComponentMap::iterator it = m_map.begin(); it != m_map.end(); ++it)
	{
		Component* ptr = it->second;
		delete ptr;
	}
	m_map.clear();
}

/*
	bool Parser::init()

	Inizializza la std::map contenente i componenti caricando in memoria un file XML 
	e chiamando un'apposita funzione.
*/
bool Parser::init()
{
	// Caricamento del file XML contenente i dati dei componenti.
	if (m_doc.LoadFile("Components.xml"))
	{
		m_root = m_doc.RootElement();
		if (!m_root)
		{
			std::cout << "Impossibile ottenere elemento di root." << std::endl;
			return false;
		}
	}
	else
	{
		std::cout << "Caricamento componenti fallito: " << m_doc.ErrorDesc() << std::endl;
		return false;
	}

	// Chiamata di funzione per inserire nella std::map i dati dei componenti.
	populateMap();

	return true;
}

/*
	void Parser::listComponents()

	Itera attraverso la std::map per stampare una lista dei componenti presenti in memoria.
*/
void Parser::listComponents()
{
	for (ComponentMap::iterator it = m_map.begin(); it != m_map.end(); ++it)
	{
		std::cout << it->first << std::endl;
	}
}

/*
	Component* Parser::getComponent(std::string name)
	
	Tramite una ricerca lineare, preso in input il nome del componente, restituisce un puntatore
	ad esso. In caso di fallimento restituisce nullptr.
*/
Component* Parser::getComponent(std::string name)
{
	Component* ptr = nullptr; // Puntatore restituito: nullo in caso di fallimento.

	// Loop di ricerca nella mappa.
	for (ComponentMap::iterator it = m_map.begin(); it != m_map.end(); ++it)
	{
		if (it->first == name)
		{
			ptr = it->second;
		}
	}

	return ptr;
}

/*
	void Parser::populateMap()

	Chiamata all'interno di init(), la funzione carica nella std::map i componenti leggendone
	le specifiche dal file XML caricato precedentemente.
*/
void Parser::populateMap()
{
	for (TiXmlElement* currentComponent = m_root->FirstChildElement(); currentComponent != nullptr;
		currentComponent = currentComponent->NextSiblingElement())
	{
		// Dati del componente in esame nel loop.
		std::string name;
		double VTPlus, VTMinus;
		double VOHigh, VOLow;

		if (currentComponent->Value() == std::string("component"))
		{
			name = currentComponent->Attribute("name");

			// Itera attraverso i parametri ed imposta:
			for (TiXmlElement* currentParameter = currentComponent->FirstChildElement(); currentParameter != nullptr;
			currentParameter = currentParameter->NextSiblingElement())
			{

				// La tensione di soglia superiore
				if (currentParameter->Value() == std::string("VTPlus"))
				{
					currentParameter->Attribute("value", &VTPlus);
				}

				// La tensione di soglia inferiore
				if (currentParameter->Value() == std::string("VTMinus"))
				{
					currentParameter->Attribute("value", &VTMinus);
				}

				// La tensione di output superiore
				if (currentParameter->Value() == std::string("VOHigh"))
				{
					currentParameter->Attribute("value", &VOHigh);
				}

				// La tensione di output inferiore
				if (currentParameter->Value() == std::string("VOLow"))
				{
					currentParameter->Attribute("value", &VOLow);
				}
			}

			Component* newPtr = new Component(name.c_str(), VTPlus, VTMinus, VOHigh, VOLow);
			m_map.insert(std::make_pair(name, newPtr));
		}

	}
}