/*

	TriggerCalc - Progetto allegato alla seconda esperienza in laboratorio.
	Calcolo del valore di resistenza o capacit� necessario data la frequenza e le specifiche del trigger.

	Giuseppe La Malfa, 4A TL.
	28/10/2015

*/

#include <iostream>
#include <conio.h>
#include <cmath>
#include <utility>

#include "Parser.h"

enum resultType
{
	CAPACITOR,
	RESISTOR,
};

const char* unitOrder[] { "p", "n", "u", "m", "", "k", "M", "g" };

/*
	std::pair<double, double> calculateAndGetAB(const double _lowerTreshold, const double _higherTreshold,
				const double _lowerOutput, const double _higherOutput)

	Calcola A e B e restituisce una std::pair che ne contiene i valori.
*/
std::pair<double, double> calculateAndGetAB (const double _lowerTreshold, const double _higherTreshold,
	const double _lowerOutput, const double _higherOutput)
{
	// Dichiarazione di variabili utilizzate per calcolare A e B.
	double ATemp, A;
	double BTemp, B;

	// Calcolo di A in due step.
	ATemp = (_lowerTreshold - _higherOutput) / (_higherTreshold - _higherOutput);
	A = log(ATemp);

	// Calcolo di B in due step.
	BTemp = (_higherTreshold - _lowerOutput) / (_lowerTreshold - _lowerOutput);
	B = log(BTemp);

	// Restituzione di una std::pair contenente i due valori, utilizzati in seguito per 
	// calcolare il valore della resistenza o del condensatore.
	return std::make_pair(A, B);
}

/*
	double calculateResult(std::pair<double, double> firstStep, const double _frequency, 
			const double _input)

	Prende come argomento i risultati di calculateAndGetAB, la frequenza, ed il valore 
	di resistenza o di capacit�. Restituisce il valore di resistenza o capacit� mancante 
	nel circuito.
*/
double calculateResult(std::pair<double, double> firstStep, const double _frequency, 
	const double _input)
{
	double result, temp;

	temp = _frequency * _input * (firstStep.first + firstStep.second);
	result = 1 / temp;

	return result;
}

/*
	void printResult(double _result, resultType _type)

	Basandosi sul risultato del calcolo della resistenza o del condensatore, la funzione
	ne processa il valore e lo stampa a video con la corretta unit� di misura.
*/
void printResult(double _result, resultType _type)
{
	int unitIndex; // Utilizzato per reperire il corretto ordine di misura dagli array globali.

	// Viene esaminato il risultato per poi adattarlo all'unit� di misura.
	if (_result < pow(10, -9))
	{
		unitIndex = 0;
		_result = _result * pow(10, -12);
	}
	else if (_result < pow(10, -6))
	{
		unitIndex = 1;
		_result = _result * pow(10, -9);
	}
	else if (_result < pow(10, -3))
	{
		unitIndex = 2;
		_result = _result * pow(10, -6);
	}
	else if (_result < pow(10, 0))
	{
		unitIndex = 3;
		_result = _result * pow(10, -3);
	}
	else if (_result < pow(10, 3))
	{
		unitIndex = 4;
		_result = _result * pow(10, 0);
	}
	else if (_result < pow(10, 6))
	{
		unitIndex = 5;
		_result = _result / pow(10, 3);
	}
	else
	{
		unitIndex = 6;
		_result = _result / pow(10, 6);
	}

	std::cout << "Per ottenere la frequenza desiderata, e' ";
	if (_type == CAPACITOR)
	{
		std::cout << "necessario un condensatore da " << _result << unitOrder[unitIndex] << std::endl;
	}
	else
	{
		std::cout << "necessaria una resistenza da " << _result << unitOrder[unitIndex] << std::endl;
	}
}

int main(int argc, char** argv)
{
	Parser parser;						// Parser utilizzato per caricare i componenti in memoria.

	std::string name;					// Nome dell'integrato.
	double frequency, input, exp;		// Dati del circuito.
	double result;						// Risultato restituito dall'algoritmo.
	resultType type;
	int temp;

	Component* comp;					// Puntatore al componente che verr� utilizzato.

	// Inizializzazione del parser.
	if (!parser.init())
	{
		_getch();
		return EXIT_FAILURE;
	}

	std::cout << " - TriggerCalc - \n";
	_getch();

	// Scelta dell'integrato.
	std::cout << "Integrati disponibili:" << std::endl;
	parser.listComponents();

	bool done = false;

	do
	{
		std::cout << "Specificare quale integrato viene utilizzato nel circuito: ";
		std::cin >> name;

		comp = parser.getComponent(name);

		if (!comp)
		{
			std::cout << "Impossibile trovare un componente con tale nome." << std::endl;
		}
		else
		{
			done = true;
		}
	} while (!done);

	do			// Il ciclo fa in modo che non vengano immessi valori invalidi.
	{
		std::cout << "Inserire il valore di (1)Resistenza o di (2)Capacita'? " << std::flush;
		std::cin >> temp;
	} while ((temp != 1) & (temp != 2));

	// Viene specificato il tipo di risultato da restituire in base alla scelta dell'utente...
	if (temp == 1)
	{
		type = CAPACITOR;
	}
	else
	{
		type = RESISTOR;
	}

	// ... cos� come anche il tipo di dato fornito dall'utente.
	if (type == CAPACITOR)
	{
		std::cout << "Inserire il valore della resistenza: " << std::flush;
	}
	else
	{
		std::cout << "Inserire il valore della capacita': " << std::flush;
	}
	
	std::cin >> input;

	do		// Input necessario per evitare di forzare l'utente a scrivere zeri di troppo.
	{
		std::cout << "Inserire l'esponente di 10: " << std::flush;
		std::cin >> exp;
	} while ((exp != -12) & (exp != -9) & (exp != -6) & (exp != -3) & (exp != 0) & (exp != 1) & (exp != 3) & (exp != 6));
	
	input = input * pow(10, exp); // Elevamento a potenza.

	std::cout << "Inserire il valore della frequenza: " << std::flush;
	std::cin >> frequency;

	// Grazie alle funzioni sopra definite � possibile ridurre l'algoritmo di calcolo ad una riga di codice.
	// Nella variabile result � presente il risultato non processato.
	result = calculateResult(calculateAndGetAB(comp->VTMinus, comp->VTPlus, comp->VOLow, comp->VOHigh), frequency, input);

	// Vengono infine stampati a video i risultati.
	printResult(result, type);

	_getch();
	return EXIT_SUCCESS;
}