#pragma once

#include <string>
#include <map>

#include "tinyxml.h"

struct Component
{
	Component(const char* _name, const double _VTPlus,
		const double _VTMinus, const double _VOHigh, const double _VOLow) :
		name(_name), VTPlus(_VTPlus), VTMinus(_VTMinus), VOHigh(_VOHigh), VOLow(_VOLow) {}

	const char* name;

	const double VTPlus, VTMinus;
	const double VOHigh, VOLow;
};

class Parser
{
	typedef std::map<std::string, Component*> ComponentMap;

	TiXmlDocument	m_doc;
	TiXmlNode*		m_root;

	ComponentMap	m_map;

public:
	Parser();
	~Parser();

	bool			init();

	void			listComponents();
	Component*		getComponent(std::string name);
		
private:

	void			populateMap();

};

